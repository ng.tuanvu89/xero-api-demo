const xero     = require('xero-node');
const config   = require('./config.json');
var fs         = require('fs');

//Private key can either be a path or a String so check both variables and make sure the path has been parsed.
if (config.privateKeyPath && !config.privateKey) {
    config.privateKey = fs.readFileSync(config.privateKeyPath);
}

console.log('=========Create Manual Journal============');
const xeroClient = new xero.PrivateApplication(config);
var sampleManualJournal = {
    Narration: "Manual Journal Entry" + Math.random(),
    Date: new Date().toISOString().split("T")[0],
    JournalLines: [
        {
            LineAmount: "-1000.00",
            AccountCode: "489"
        },
        {
            LineAmount: "1000.00",
            AccountCode: "620"
        }
    ]
};

var manualjournal = xeroClient.core.manualjournals.newManualJournal(sampleManualJournal);

manualjournal.save()
.then(function(res) {
    //Manual Journal has been created 
    console.log('=========Manual Journal entry created successfully============');
    console.log(res.response);
})
.catch(function(err) {
    //Some error occurred
    console.log(err);
});
# This is the demo for creating and retrieving Manual Journal Entry of Xero system. Please following the steps below to setup and running the demo:

Assumtions: 
- You already registered a Xero account.
- You are using Mac OS.
- You already downloaded and extracted the xero_demo.zip

## 1. Prepare public/private keys for creating Private Application in Xero:
- First, open your terminal application and go to the demo script folder:
```
cd [path_to_the_demo_script_directory]
```
- We will use OpenSSL to generate the keys (this software is shiped along with MacOS), please run the following commands in your terminal:
```
openssl genrsa -out privatekey.pem 1024
openssl req -new -x509 -key privatekey.pem -out publickey.cer -days 1825
openssl pkcs12 -export -out public_privatekey.pfx -inkey privatekey.pem -in publickey.cer
```

You will be asked to enter some private information when running above commands.
Now checking the demo directory, you will see 3 files:   
+ privatekey.pem (this will used by the demo script)
+ publickey.cer (this will use for creating the Private Application in Xero system)
+ public_privatekey.pfx

** Note: if you are uing nother system, please view this document for more info about installing OpenSSL: 
https://developer.xero.com/documentation/api-guides/create-publicprivate-key

## 2. Create Private Application in Xero:
- Login to the Xero Developer portal which is located at https://api.xero.com
- Go to the My Applications > Add Application screen in the Xero Developer portal to add your application.
- Select “Private” and enter a name for your application
- Choose your organisation from the drop down list
- Upload the public certificate (.cer file) you generated in step one.
- Choose save.
- Now you will see your Application information, the OAuth Credentials section will show your Consumer Key that will used by the api

## 3. Update demo configurations:
- Edit the file ```config.json``` in the demo directory
- Update consumerKey by the keys in step 2.
- Update privateKeyPath to your privatekey.pem which generated in step 1.

## 4. Run the demo with nodejs:
- Install script's dependences:
```npm install```
- Running the demo script to call xero api and create the entry then view the result:
```node create_entry.js```
